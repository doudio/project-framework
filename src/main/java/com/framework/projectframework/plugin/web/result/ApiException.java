package com.framework.projectframework.plugin.web.result;

import com.framework.projectframework.plugin.web.enums.SystemEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class ApiException extends RuntimeException {

    private int code;
    private String msg;

    public ApiException(){
    }

    public ApiException(ApiException e){
        this.code = e.getCode();
        this.msg = e.getMsg();
    }

    public ApiException(SystemEnum systemEnum) {
        this(systemEnum.getCode(), systemEnum.getMsg());
    }

    public ApiException(int code,String msg) {
        this.code = code;
        this.msg = msg;
    }

}
