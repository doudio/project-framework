package com.framework.projectframework.plugin.web.parameter;

import com.framework.projectframework.plugin.web.enums.SystemEnum;
import com.framework.projectframework.plugin.web.result.ApiException;
import org.springframework.validation.BindingResult;

/**
 * 验证前端传入参数是否合法
 */
public class ParamUtils {

    public static void validation(BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            return;
        }
        /*StringBuilder stringBuilder = new StringBuilder();
        List<FieldError> allErrors = bindingResult.getFieldErrors();
        allErrors.stream().forEach(e -> {
            stringBuilder.append(e.getField()).append("::").append(e.getDefaultMessage());
        });*/
        throw new ApiException(SystemEnum.PARAM_ERROR);
    }

}
