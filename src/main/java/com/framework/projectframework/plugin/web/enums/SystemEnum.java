package com.framework.projectframework.plugin.web.enums;

import lombok.Getter;

@Getter
public enum SystemEnum {

    UNKNOWN(99999, "未知错误"),
    SUCCESS(0, "操作成功"),
    PARAM_ERROR(4000, "参数错误"),
    METHOD_ERROR(4001,"请求方式错误");

    private int code;
    private String msg;

    SystemEnum(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

}
