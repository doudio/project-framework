package com.framework.projectframework.plugin.web.result;

import lombok.Data;

/**
 * @author: quwenlei
 * @date: 2020-04-09 14:47
 * @description:
 **/
@Data
public class PageResult<T> extends ApiResult<T> {

    private long total;//总页数
    private long totalPages;//总页数
    private long pageSize;//页面容量
    private long page;//当前页
    private String orderBy;
    private Boolean isAsc;

    public PageResult() {
    }

    /**
     * @param data       数据列表
     * @param total      数据总量
     * @param totalPages 总页数
     * @param pageSize   页容量
     * @param page       当前页
     * @Description: 分页回调数据
     **/
    public PageResult success(T data, long total, long totalPages, long pageSize, long page) {
        super.success(data);

        this.total = total;
        this.totalPages = totalPages;
        this.pageSize = pageSize;
        this.page = page;

        return this;
    }

    public PageResult<T> success(T list, long total, long current, long size, long pages, String orderBy, Boolean isAsc) {
        success(list, total, current, size, pages);
        this.orderBy = orderBy;
        this.isAsc = isAsc;
        return this;
    }

}
