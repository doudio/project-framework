package com.framework.projectframework.plugin.web.result;

import com.framework.projectframework.plugin.web.enums.SystemEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class ApiResult<T> {

    private int code;
    private String msg;
    private T data;

    public ApiResult success() {
        return setEnum(SystemEnum.SUCCESS);
    }

    public ApiResult success(T data) {
        ApiResult apiResult = setEnum(SystemEnum.SUCCESS);
        apiResult.setData(data);
        return apiResult;
    }

    public ApiResult exception(SystemEnum systemEnum, T data) {
        this.code = systemEnum.getCode();
        this.msg = systemEnum.getMsg();
        this.data = data;
        return this;
    }

    public ApiResult exception(SystemEnum systemEnum) {
        return exception(systemEnum, null);
    }


    public ApiResult setEnum(SystemEnum systemEnum){
        this.code = systemEnum.getCode();
        this.msg = systemEnum.getMsg();
        return this;
    }

    public ApiResult exception(ApiException e) {
        this.code = e.getCode();
        this.msg = e.getMsg();
        return this;
    }

}
