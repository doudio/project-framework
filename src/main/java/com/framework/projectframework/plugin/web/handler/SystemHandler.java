package com.framework.projectframework.plugin.web.excetion.handler;

import com.framework.projectframework.plugin.web.enums.SystemEnum;
import com.framework.projectframework.plugin.web.result.ApiException;
import com.framework.projectframework.plugin.web.result.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class SystemHandler {

    /**
     * 捕捉请求方式错误
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ApiResult httpRequestMethodNotSupportedExceptionHandler(HttpRequestMethodNotSupportedException e) {
        return new ApiResult().setEnum(SystemEnum.METHOD_ERROR);
    }

    /**
     * 捕捉系统自定义异常
     */
    @ExceptionHandler(ApiException.class)
    public ApiResult systemExceptionHandler(ApiException e) {
        log.error("SystemHandler捕捉到ApiException!", e);

        return new ApiResult().exception(e);
    }

    /**
     * 捕捉其他异常
     */
    @ExceptionHandler
    public ApiResult systemExceptionHandler(Exception e) {
        log.error("SystemHandler捕捉到Exception!", e);

        return new ApiResult().setEnum(SystemEnum.UNKNOWN);
    }

}
