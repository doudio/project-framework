package com.framework.projectframework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectFrameworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectFrameworkApplication.class, args);
    }

}
