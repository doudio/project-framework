package com.framework.projectframework.aop;

import com.framework.projectframework.plugin.web.parameter.ParamUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

/**
 * 使用aop获取到所有Controller中的 BindingResult 进行验证
 **/
@Aspect
@Component
public class ParamValidationAOP {

    @Pointcut("execution(* com.framework.*.contoller.*Controller.*(..))")
    public void allController() {
    }

    @Before("allController()")
    public void doBefore(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            if (arg instanceof BindingResult) ParamUtils.validation((BindingResult) arg);
        }
    }

}
