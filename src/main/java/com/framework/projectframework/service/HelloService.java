package com.framework.projectframework.service;

import com.framework.projectframework.web.vo.form.HelloForm;
import com.framework.projectframework.web.vo.UserVO;

import java.util.List;

public interface HelloService {

    List<UserVO> hello(HelloForm helloForm);

}
