package com.framework.projectframework.service.impl;

import com.framework.projectframework.domain.User;
import com.framework.projectframework.web.vo.form.HelloForm;
import com.framework.projectframework.mappers.UserMapper;
import com.framework.projectframework.service.HelloService;
import com.framework.projectframework.web.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class HelloServiceImpl implements HelloService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<UserVO> hello(HelloForm helloForm) {
        List<User> users = userMapper.selectList(null);

        List<UserVO> collect = users.stream().map(e -> {
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(e, userVO);
            return userVO;
        }).collect(Collectors.toList());

        return collect;
    }

}
