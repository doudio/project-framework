package com.framework.projectframework.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.framework.projectframework.domain.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {
}
