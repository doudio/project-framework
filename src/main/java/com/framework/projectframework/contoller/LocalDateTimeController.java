package com.framework.projectframework.contoller;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 测试 LocalDateTime
 */
@RestController
@RequestMapping("date")
public class LocalDateTimeController {

    @Data
    @Builder
    @AllArgsConstructor
    public static class LocalDateTimeForm {
        private LocalDate date;
        private LocalDateTime dateTime;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LocalDateTimeVO {

        private LocalDateTime localDateTime;

        private LocalDateTimeForm form;

        @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
        private LocalDateTime jsonFormat;

        private Date date;

    }

    @GetMapping
    public LocalDateTimeVO parse(LocalDateTimeForm form) {
        LocalDateTime now = LocalDateTime.now();
        return LocalDateTimeVO.builder().localDateTime(now).jsonFormat(now).date(new Date()).form(form).build();
    }

}
