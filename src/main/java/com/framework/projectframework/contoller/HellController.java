package com.framework.projectframework.contoller;

import com.framework.projectframework.web.vo.form.HelloForm;
import com.framework.projectframework.plugin.web.result.ApiException;
import com.framework.projectframework.plugin.web.result.ApiResult;
import com.framework.projectframework.plugin.web.result.PageResult;
import com.framework.projectframework.service.HelloService;
import com.framework.projectframework.web.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * 控制层
 */
@RestController
public class HellController {

    @Autowired
    private HelloService helloService;

    @GetMapping("hello")
    public ApiResult hello(HelloForm helloForm, BindingResult bindingResult) {
        List<UserVO> hello = helloService.hello(helloForm);

        return new ApiResult().success(hello);
    }

    @PostMapping("postHello")
    public ApiResult postHello(@Valid HelloForm helloForm, BindingResult bindingResult) {
        return new PageResult().success();
    }

    @GetMapping("throws")
    public ApiResult getThrows(@Valid HelloForm helloForm, BindingResult bindingResult) throws Exception {
        String temp = helloForm.getTemp();
        if ("exception".equals(temp)){
            throw new Exception();
        } else if ("apiException".equals(temp)) {
            throw new ApiException(666, "自定义测试异常");
        }

        return new ApiResult().success();
    }

    @PostMapping("throws")
    public ApiResult postThrows(@Valid HelloForm helloForm, BindingResult bindingResult) {
        String temp = helloForm.getTemp();
        if ("apiException".equals(temp)) {
            throw new ApiException(666, "自定义测试异常");
        }

        return new ApiResult().success();
    }

}
