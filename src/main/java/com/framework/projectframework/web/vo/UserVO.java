package com.framework.projectframework.web.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class UserVO {

    @TableId
    private Integer id;

    private String account;
    private String userName;

}
