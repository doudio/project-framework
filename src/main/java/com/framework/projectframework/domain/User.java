package com.framework.projectframework.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 数据实体类
 */
@Data
@TableName("tbl_user")
public class User {

    @TableId
    private Integer id;

    private String account;
    private String userName;
    private String password;

}
