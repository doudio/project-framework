package com.framework.projectframework.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: quwenlei
 * @date: 2020-04-09 15:00
 * @description:
 **/
@Configuration
public class SpringWebConfig implements WebMvcConfigurer {
}
