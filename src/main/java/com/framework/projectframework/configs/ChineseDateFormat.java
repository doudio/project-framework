package com.framework.projectframework.configs;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

/**
 * 这样配置不兼容 ChineseLocalDateTimeFormat
 * spring.jackson.date-format=com.framework.projectframework.configs.ChineseDateFormat
 *
 * @see com.framework.projectframework.configs.ChineseLocalDateTimeFormat
 */
@Data
//@Deprecated
public class ChineseDateFormat extends SimpleDateFormat {

    private static final long serialVersionUID = 6276491293753307748L;

    public ChineseDateFormat() throws Exception {
        super("y-MM-dd HH:mm:ss", Locale.CHINESE);
        setTimeZone(TimeZone.getTimeZone("GMT+8"));
    }

}