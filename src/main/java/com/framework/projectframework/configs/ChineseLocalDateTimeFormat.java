package com.framework.projectframework.configs;

import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * LocalDateTime 格式化
 *
 * 添加配置
 * spring.jackson.locale=en_CH
 * spring.jackson.time-zone=Asia/Shanghai
 * spring.jackson.date-format=yyyy-MM-dd HH:mm:ss
 *
 * @see <a href="https://www.baeldung.com/spring-boot-formatting-json-dates">https://www.baeldung.com/spring-boot-formatting-json-dates</a>
 * @see <a href="https://janus.blog.csdn.net/article/details/107065047">https://janus.blog.csdn.net/article/details/107065047</a>
 * @see <a href="https://segmentfault.com/a/1190000022512201">https://segmentfault.com/a/1190000022512201</a>
 */
@Configuration
public class ChineseLocalDateTimeFormat {

    @Value("${spring.jackson.date-format:yyyy-MM-dd HH:mm:ss}")
    private String dateTimeFormat;

    private static final String dateFormat = "yyyy-MM-dd";

    /**
     * 作为返回的 格式化
     * @return
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {
        return builder -> {
            builder.simpleDateFormat(dateTimeFormat);
            builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern(dateFormat)));
            builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(dateTimeFormat)));
        };
    }

    /**
     * 作为参数的 日期格式化
     */
    @Bean
    public Formatter<LocalDate> localDateFormatter() {
        return new Formatter<LocalDate>() {

            private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat);

            @Override
            public LocalDate parse(String text, Locale locale) throws ParseException {
                return LocalDate.parse(text, dateTimeFormatter);
            }

            @Override
            public String print(LocalDate localDate, Locale locale) {
                return dateTimeFormatter.format(localDate);
            }

        };
    }

    /**
     * 作为参数的 日期时间格式化
     */
    @Bean
    public Formatter<LocalDateTime> localDateTimeFormatter() {
        return new Formatter<LocalDateTime>() {

            private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);

            @Override
            public LocalDateTime parse(String text, Locale locale) throws ParseException {
                return LocalDateTime.parse(text, formatter);
            }

            @Override
            public String print(LocalDateTime localDateTime, Locale locale) {
                return formatter.format(localDateTime);
            }

        };
    }

}